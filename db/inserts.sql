INSERT INTO integrantes (id, matricula, nombre, apellido, activo) VALUES
('1', 'Y25495', 'Sebastian', 'Pereira', 1),
('2', 'Y19937', 'Juan', 'Aquino', 1),
('3', 'Y25387', 'Junior', 'Cabral', 1),
('4', 'Y19099', 'Elvio', 'Aguero', 1),
('5', 'UG0085', 'Luis', 'Delgado', 1);

INSERT INTO media (id, src, url, idIntegrante, idTipomedia, alt, activo) VALUES
('1', NULL, 'https://www.youtube.com/embed/FRn6xXXF-7s?si=yVJP3egE0MB4siuE', 'Y25495', '1', 'Trailer de attack of titan', 1),
('2','/assets/Sebastian-Dibujo.jpg', NULL, 'Y25495', '2', 'Dibujo una fogata', 1),
('3','/assets/Sebastian-foto.webp', NULL, 'Y25495', '3', 'Dark Souls', 1),

('4',NULL, 'https://www.youtube.com/embed/_Yhyp-_hX2s', 'Y19937', '1', 'Canción de Eminem', 1),
('5', '/assets/Juan-Dibujo.jpg', NULL, 'Y19937', '2', 'canasta de basketball', 1),
('6','/assets/Juan-foto.jpeg', NULL, 'Y19937', '3', 'aro de basketball', 1),

('7',NULL, 'https://www.youtube.com/embed/Tsnyq-3k7Bg?si=-Bzir8axv4WRU4MS&controls=0', 'Y25387', '1','Video de Quantum fracture', 1),
('8','/assets/Junior-Dibujo.png', NULL, 'Y25387', '2','Dibujo de jupiter', 1),
('9','/assets/Junior - Foto.jpg', NULL, 'Y25387', '3', 'Atardecer', 1),

('10',NULL, 'https://www.youtube.com/embed/NynNdkY2gx0?si=nzQjEF5y6hY6uSOp', 'Y19099', '1', 'Video de jugadas de futbol', 1),
('11','/assets/Elvio-Dibujo.png', NULL, 'Y19099', '2', 'Dibujo de un partido de futbol', 1),
('12','/assets/Elvio-Foto.jpg', NULL, 'Y19099', '3', 'El bicho, Cristiano Ronaldo', 1),

('13',NULL, 'https://www.youtube.com/embed/Hv5ET7azgEk?si=WxryTH3oIMGTTD-Z', 'UG0085', '1', 'Video de la vida secreta de la mente', 1),
('14','/assets/Luis Delgado-2.png', NULL, 'UG0085', '2', 'Dibujo de una PC', 1),
('15','/assets/Luis Delgado.jpeg', NULL, 'UG0085', '3','Foto personal en un cerro', 1);


INSERT INTO tipoMedia (id, nombre, activo) VALUES
( '1', 'Youtube', 1),
('2', 'Dibujo', 1),
('3', 'Imagen', 1);

INSERT INTO usuarios (email, contrasenha, super_usuario, matricula) VALUES
('admin@gmail.com', '$2b$10$nlSz1AMKAiF.czm5dNtaJe5j8FAltTFezQfnewJQe2rn8Vh1wdy9m', 1, 'Y25495');
