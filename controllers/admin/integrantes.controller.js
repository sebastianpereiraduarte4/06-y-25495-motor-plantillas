const integranteStoreSchema = require("../../validators/integrantes/create")
const integranteUpdateSchema = require("../../validators/integrantes/edit");
const IntegranteModel = require("../../models/integrante.model");
const MediaModel = require("../../models/media.model");

//index - listado

const IntegrantesController = {
    //definición del objeto
    index:  function (req, res) {
        IntegranteModel
            .getAll(req)
            .then((integrantes) => {
                console.log("integrantes", integrantes);
                res.render("admin/integrantes/index", {
                    integrantes: integrantes,
                });
            })
            .catch((error) => {
                console.log("error", error)
            })
    },

    crearForm: function (req, res) {
        res.render("admin/integrantes/crearForm", {
            /*
            matricula: req.query.matricula,
            nombre: req.query.nombre,
            apellido: req.query.apellido,
            activo: req.query.activo

             */
        });
    },

    store: async function (req, res) {
        const {error} = integranteStoreSchema.validate(req.body);
        const matricula = req.body.matricula;

        if (error) {
            const errorMessages = error.details.map(detail => detail.message);
            res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent(errorMessages.join(';'))}&matricula=${encodeURIComponent(req.body.matricula)}&nombre=${encodeURIComponent(req.body.nombre)}&apellido=${encodeURIComponent(req.body.apellido)}`);
        } else if (await IntegranteModel.getByField('integrantes', 'matricula', matricula)) {
            res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent('¡Ya existe un integrante con la matrícula que has introducido!')}`);
        } else {
            try {
                await IntegranteModel.create(req.body);
                res.redirect(`/admin/integrantes/listar?success=${encodeURIComponent('¡Integrante creado correctamente!')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    },


//operación de edición

    update: async function (req, res) {
        const {error} = integranteUpdateSchema.validate(req.body);
        const matricula = req.params.matricula;

        if (error) {
            const errorMessages = error.details.map(detail => detail.message);
            res.redirect("/admin/integrantes/edit/${matricula}?error=${encodeURIComponent(errorMessages.join(';'))}");
        } else {
            try {
                await IntegranteModel.update(req.body, matricula);
                res.redirect("/admin/integrantes/listar?success=" + encodeURIComponent('¡Integrante actualizado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al actualizar al integrante!'));
            }
        }
    },


    //el metodo que renderiza la vista que contiene el formulacio de edición
    //ademas de enciarle los datos que tiene que precargar en dicho formulario
    edit: async function (req, res) {
        const matricula = req.params.matricula;
        try {
            const integrante = await IntegranteModel.getByid(matricula);
            res.render("admin/integrantes/editForm", {
                integrante: integrante
            });
        } catch (err) {
            console.log("errormabsfdgkjbak", err);
            res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al obtener al integrante!'));
        }
    },

    //Método destroy para borrado lógico
    destroy: async function (req, res) {
        const matricula = req.params.matricula;
        const matriculaAsociada = await MediaModel.getByField('media', 'idIntegrante', matricula);

        if (matriculaAsociada) {
            res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡No se puede eliminar el integrante porque tiene media asociada!'));
        } else {
            try {
                await IntegranteModel.delete(matricula);
                res.redirect("/admin/integrantes/listar?success=" + encodeURIComponent('¡Integrante eliminado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
        }
    },
};



module.exports = IntegrantesController;