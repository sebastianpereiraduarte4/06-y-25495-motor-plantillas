const fs = require('fs').promises;
const mediaStoreSchema = require('../../validators/media/create');
const mediaUpdateSchema = require('../../validators/media/edit');
const MediaModel = require("../../models/media.model");
const TipoMediaModel = require("../../models/tipo_media.model");
const IntegranteModel = require("../../models/integrante.model");

const MediaController = {

    index: async function (req, res) {
        try {
            const media = await MediaModel.getAll(req);
            const integrantes = await IntegranteModel.getIntegrante();
            const tipoMedia = await TipoMediaModel.getTipoMedia();
            res.render("admin/media/index", {
                media: media,
                integrantes: integrantes,
                tipoMedia: tipoMedia,
            });
        } catch (error) {
            console.log(error);
            res.redirect("/admin/media/listar?error=" + encodeURIComponent('¡Error al obtener los medias'));
        }
    },

    crearForm: async function (req, res) {
        const integrantes = await IntegranteModel.getIntegrante();
        const tipoMedia = await TipoMediaModel.getTipoMedia();
        res.render("admin/media/crearForm", {
            integrantes: integrantes,
            tipoMedia: tipoMedia,
            tipoMediaSeleccionado: req.query.idTipomedia,
            url: req.query.url,
            alt: req.query.alt,
            integranteSeleccionado: req.query.idIntegrante,
        });
    },

    store: async function (req, res) {
        const {error}  = mediaStoreSchema.validate(req.body);

        if (error) {
            const errorMessages = error.details.map(detail => detail.message);
            res.redirect(`/admin/media/crear?error=${encodeURIComponent(errorMessages.join(';'))}
            &idTipomedia=${encodeURIComponent(req.body.idTipoMedia)}
            &url=${encodeURIComponent(req.body.url)}
            &alt=${encodeURIComponent(req.body.alt)}
            &idIntegrante=${encodeURIComponent(req.body.idIntegrante)}`);
        } else {
            let srcPath = '';
            if (req.file) {
                var tpm_path = req.file.path;
                var destino = "public/assets/" + req.file.originalname;
                try {
                    await fs.rename(tpm_path, destino);
                    srcPath = "/assets/" + req.file.originalname;
                } catch (err) {
                    return res.sendStatus(500);
                }
            }
            try {
                await MediaModel.create(req.body, srcPath);
                res.redirect(`/admin/media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/media/crearForm?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    },


    update: async function (req, res) {
        const {error} = mediaUpdateSchema.validate(req.body);
        const id = parseInt(req.params.id);
        const existingMedia = await MediaModel.getById(id);

        let errores = [];

        if (existingMedia.src && req.body.url) {
            errores.push('¡No puedes agregar una URL porque ya existe un SRC!');
        }

        if (existingMedia.url && req.file) {
            errores.push('¡No puedes agregar un SRC porque ya existe una URL!');
        }

        if (error || errores.length > 0) {
            const errorMessages = error? error.details.map(detail => detail.message) : [];
            errores = errores.concat(errorMessages);
            res.redirect(`/admin/media/edit/${id}?error=${encodeURIComponent(errores.join(';'))}`);
        } else {
            let srcPath = '';
            if (existingMedia.src) {
                srcPath = existingMedia.src;
            }
            if (req.file) {
                var tpm_path = req.file.path;
                var destino = "public/assets/images/" + req.file.originalname;
                try {
                    await fs.rename(tpm_path, destino);
                    srcPath = "/assets/" + req.file.originalname;
                } catch (err) {
                    return res.sendStatus(500);
                }
            }
            try {
                await MediaModel.update(req.body, id, srcPath);
                res.redirect("/admin/media/listar?success=" + encodeURIComponent('¡Media actualizado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/media/editForm/" + id + "?error=" + encodeURIComponent('¡Error al actualizar el registro!'));
            }
        }
    },


    edit: async function (req, res) {
        const id = parseInt(req.params.id);
        const integrantes = await IntegranteModel.getIntegrante();
        const tipoMedia = await TipoMediaModel.getTipoMedia();
        try {
            const media = await MediaModel.getById(id);
            res.render("admin/media/editForm", {
                integrantes: integrantes,
                tipoMedia: tipoMedia,
                media: media
            });
        } catch (err) {
            console.log(err);
            res.redirect("/admin/media/listar?error=" + encodeURIComponent('¡Error al obtener el registro!'));
        }
    },



    destroy: async function (req, res) {
        const id = parseInt(req.params.id);
        try {
            await MediaModel.delete(id);
            res.redirect("/admin/media/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
        } catch (err) {
            console.log(err);
            res.redirect("/admin/media/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
        }
    },
};

module.exports = MediaController;