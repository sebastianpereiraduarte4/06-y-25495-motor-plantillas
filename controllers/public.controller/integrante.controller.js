const express = require("express");
const router = express.Router();
require("dotenv").config();
//const dbSqlite = require("../db/conexion");
//const routerAdmin = require("../routes/admin");
const {getAll} = require("../../db/conexion");

const {getIntegrante, getAllMatriculas, getByMatricula} = require("../../models/integrante.model");
const {getTipoMedia} = require("../../models/tipo_media.model");
const {getMediaByMatricula} = require("../../models/media.model");


const IntegranteController = {
    index: async (req, res, next) => {
        const matriculas = (await getAllMatriculas()).map(obj => obj.matricula);
        const tipoMedia = await getTipoMedia();
        const matricula = req.params.matricula;
        const integrantes = await getIntegrante();

        if (matriculas.includes(matricula)) {
            console.log("aaaaaa en la paginas")
            const integranteFilter = await getByMatricula(matricula);
            const mediaFilter = await getMediaByMatricula(matricula);
            res.render('integrante', {
                integrante: integranteFilter,
                tipoMedia: tipoMedia,
                media: mediaFilter,
                integrantes: integrantes
            });
        } else {
            next();
        }
    }
};

module.exports = IntegranteController;