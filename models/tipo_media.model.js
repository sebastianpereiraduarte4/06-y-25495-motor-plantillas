const {db, getAll, run, matriculaExistente, getLastId} = require("../db/conexion");
const TipoMediaModel ={

    //getAll - obtener todos los registros - filtros opcionales de búsqueda deben estar disponibles por cada campo de la tabla
    async getAll(req) {
        let query = "SELECT * FROM tipoMedia WHERE activo = 1";
        let queryParams = [];

        for (const prop in req.query["s"]) {
            if (req.query["s"][prop]) {
                query += ` AND ${prop} LIKE? `;
                queryParams.push(`%${req.query["s"][prop]}%`);
            }
        }

        return new Promise((resolve, reject) => {
            db.all(query, queryParams, (error, tipoMedia) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(tipoMedia);
                    console.log("tipoMedia", tipoMedia)
                }
            });
        });
    },

    //getByid - obtener un registro por id
    getById(id) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM tipoMedia WHERE id = ?`, [id], (error, tipoMedia) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(tipoMedia);
                }
            });
        });
    },

    getTipoMedia() {
        return new Promise((resolve, reject) => {
            db.all("SELECT * FROM tipoMedia WHERE activo = 1 ORDER BY id", (error, tipoMedia) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(tipoMedia);
                }
            });
        });
    },


    //crear registro
    async create(req) {

        return new Promise((resolve, reject) => {
            db.run("insert into tipoMedia (id, nombre, activo) values (?, ?, ?)",[req.nombre, req.activo], (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

    //actualizar registro
    update(req, id) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE tipoMedia SET nombre = ? WHERE id = ?", [req.nombre, id], error => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

    //borrar registro
    delete(id) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE tipoMedia SET activo = 0 WHERE id = ?", [id], error => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

}

module.exports = TipoMediaModel;