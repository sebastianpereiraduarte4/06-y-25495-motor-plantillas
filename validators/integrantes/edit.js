const Joi = require("joi");

const integranteUpdateSchema = Joi.object({
    //se colocan los campos y las reglas de validación que se aplica en cada campo
    nombre: Joi.string()
        .required()
        .min(4)
        .regex(/^[a-zA-Z-]*$/)
        .messages({
            "string.base": '"nombre" debe ser una cadena ',
            "string.empty": '"nombre" es obligatorio ',
            "string.min": '"nombre" debetener al menos 4 caracteres ',
            "string.pattern.base": '"nombre" solo acepta caracteres de la A a la Z ',
            "any.required": '"nombre" es obligatorio ',
        }),

    apellido: Joi.string()
        .required()
        .min(4)
        .regex(/^[a-zA-Z-]+$/)
        .messages({
            "string.base": '"apellido" debe ser una cadena ',
            "string.empty": '"apellido" es obligatorio ',
            "string.min": '"apellido" debetener al menos 4 caracteres ',
            "string.pattern.base": '"apellido" solo acepta caracteres de la A a la Z ',
            "any.required": '"apellido" es obligatorio ',
        }),

    matricula: Joi.string()
        .required()
        .min(2)
        .regex(/^[a-zA-Z0-9]*$/)
        .messages({
            "string.base": '"matricula" debe ser una cadena ',
            "string.empty": '"matricula" es obligatorio ',
            "string.min": '"matricula" debetener al menos 4 caracteres ',
            "string.pattern.base": '"matricula" solo acepta caracteres de la A a la Z y numeros del 1 al 9 ',
            "any.required": '"matricula" es obligatorio ',
        }),


}).options({abortEarly: false});

module.exports = integranteUpdateSchema;