const Joi = require("joi");

const mediaStoreSchema = Joi.object({
    //se colocan los campos y las reglas de validación que se aplica en cada campo
    src: Joi.string()
        .optional(),

    url: Joi.string()
        .optional(),

    alt: Joi.string()
        .required()
        .max(80)
        .regex(/^[a-zA-Z0-9]*$/)
        .messages({
            "string.base": '"alt" debe ser una cadena ',
            "string.empty": '"alt" es obligatorio ',
            "string.max": '"alt" debe tener maximo 80 caracteres ',
            "string.pattern.base": '"alt" solo acepta caracteres de la A a la Z y del 1 al 9',
            "any.required": '"alt" es obligatorio ',
        }),

    idIntegrante: Joi.string()
        .required()
        .min(4)
        .regex(/^[a-zA-Z0-9]*$/)
        .messages({
            "string.base": '"idIntegrante" debe ser una cadena de caracteres ',
            "string.empty": '"idIntegrante" es obligatorio ',
            "string.min": '"idIntegrante" debetener al menos 4 caracteres ',
            "string.pattern.base": '"idIntegrante" solo acepta caracteres de la A a la Z y numeros del 1 al 9 ',
            "any.required": '"idIntegrante" es obligatorio ',
        }),

    idTipomedia: Joi.string()
        .required()
        .min(1)
        .regex(/^[0-9]*$/)
        .messages({
            "string.base": '"idTipomedia" debe ser numerico ',
            "string.empty": '"idTipomedia" es obligatorio ',
            "string.min": '"idTipomedia" debetener al menos 1 caracteres ',
            "string.pattern.base": '"idTipomedia" solo acepta numeros ',
            "any.required": '"idTipomedia" es obligatorio ',
        }),

    activo:
        Joi.required()
        .messages({
            "any.required": '"activo" es obligatorio ',
        }),

}).options({abortEarly: false});

module.exports = mediaStoreSchema;