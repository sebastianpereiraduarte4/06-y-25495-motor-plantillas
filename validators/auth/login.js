const Joi = require('joi');

const loginSchema = Joi.object({
    email:
        Joi.string()
            .email()
            .required()
            .messages({
                'string.empty': '¡El email no puede estar vacío!',
                'string.email': '¡El email no es válido!',
            }),

    contrasenha:
        Joi.string()
            .required()
            .messages({
                'string.empty': '¡La contraseña no puede estar vacía!',
            }),
}).options({abortEarly: false});

module.exports = loginSchema;