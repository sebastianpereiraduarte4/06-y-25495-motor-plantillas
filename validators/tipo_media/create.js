const Joi = require("joi");

const tipoMediaStoreSchema = Joi.object({
    //se colocan los campos y las reglas de validación que se aplica en cada campo
    nombre: Joi.string()
        .required()
        .min(4)
        .regex(/^[a-zA-Z-]*$/)
        .messages({
            "string.base": '"nombre" debe ser una cadena ',
            "string.empty": '"nombre" es obligatorio ',
            "string.min": '"nombre" debetener al menos 4 caracteres ',
            "string.pattern.base": '"nombre" solo acepta caracteres de la A a la Z ',
            "any.required": '"nombre" es obligatorio ',
        }),

    activo: Joi.string()
        .required()
        .messages({
            "any.required": '"activo" es obligatorio ',
        }),

}).options({abortEarly: false});

module.exports = tipoMediaStoreSchema;